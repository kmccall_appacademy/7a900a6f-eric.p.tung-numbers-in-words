require 'byebug'

class Fixnum
  def initialize
  end

  ONES = {
   0 => "zero",
   1 => "one",
   2 => "two",
   3 => "three",
   4 => "four",
   5 => "five",
   6 => "six",
   7 => "seven",
   8 => "eight",
   9 => "nine"
  }

  TENS = {
   20 => "twenty",
   30 => "thirty",
   40 => "forty",
   50 => "fifty",
   60 => "sixty",
   70 => "seventy",
   80 => "eighty",
   90 => "ninety"
  }

  TEENS = {
   10 => "ten",
   11 => "eleven",
   12 => "twelve",
   13 => "thirteen",
   14 => "fourteen",
   15 => "fifteen",
   16 => "sixteen",
   17 => "seventeen",
   18 => "eighteen",
   19 => "nineteen"
  }

  MAGNITUDE = {
    0 => "",
    1 => "thousand",
    2 => "million",
    3 => "billion",
    4 => "trillion",
  }

  def in_words_2_digits
    if self < 10
      ONES[self]
    elsif self < 20
      TEENS[self]
    elsif self < 100
      if self%10 == 0
        TENS[self]
      else
        TENS[self/10*10] + ' ' + ONES[self%10]
      end
    end
  end

  def in_words_3_digits
    result = ""
    hundreds_digit = self/100
    rest_of_number = self - hundreds_digit*100
    if hundreds_digit!=0
      result += ONES[hundreds_digit]+ " hundred"
    end
    if rest_of_number !=0
      result += " " if result !=""
      result += "#{rest_of_number.in_words_2_digits}"
    end
    if rest_of_number ==0 && result ==""
      result += "#{0.in_words_2_digits}"
    end
    result
  end

  def find_magnitude
    # finds largest index of MAGNITUDE
    (self.to_s.length - 1 ) / 3
  end

  def in_words
    result=""
    temp_num = self
    while temp_num!=0
      result += " " if result !=""
      mag = temp_num.find_magnitude
      sub_num = temp_num / (10**(3*mag))
      if sub_num!=0
        result += "#{sub_num.in_words_3_digits}"
        result += " #{MAGNITUDE[mag]}" if mag > 0
      end
      temp_num-=sub_num*(10**(3*mag))
    end
    if temp_num==0 && result ==""
      result += "#{0.in_words_3_digits}"
    end
    result
  end




end
